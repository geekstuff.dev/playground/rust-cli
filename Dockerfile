# Build stage
FROM rust:latest AS build

# Add linux toolchain and requirements
RUN rustup target add x86_64-unknown-linux-musl
RUN apt-get update && apt-get install -y musl-tools musl-dev
RUN update-ca-certificates

# Build setup
WORKDIR /app

# Copy files allowed by .dockerignore
COPY . .

# Build
RUN cargo build --target x86_64-unknown-linux-musl --release

# Intermediary stage
FROM alpine AS intermediary

# Create app user
RUN adduser \
    --disabled-password \
    --gecos "" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "1000" \
    "app"

# Final stage
FROM scratch

# Import app user
COPY --from=intermediary /etc/passwd /etc/passwd
COPY --from=intermediary /etc/group /etc/group

# Copy our built app
ARG APP_NAME="app"
COPY --from=build /app/target/x86_64-unknown-linux-musl/release/${APP_NAME} /app

# Use app user
USER app:app

# Send every (or no) commands to our app
ENTRYPOINT ["/app"]
