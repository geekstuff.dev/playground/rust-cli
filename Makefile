#!make

all: versions
all: fmt
all: check
all: clippy
# all: test
all: build-all
all: run-bin

-include ${DC_ASSETS_RUST}/app.mk

DOCKER_IMAGE ?= rust-cli

# OPT ?=--release
OPT ?=
TARGET ?= $(shell test "${OPT}" = "--release" && echo "release" || echo "debug")

build-all: build-linux
build-all: build-windows

build-linux:
	cargo build --target x86_64-unknown-linux-musl ${OPT}
	@echo "> WSL Test command in distro ${WSL_DISTRO_NAME}:"
	@OPT=${OPT} $(MAKE) .wsl-test-command

.wsl-test-command: BIN_PATH ?= ${HOST_WORKSPACE_FOLDER}/target/x86_64-unknown-linux-musl/${TARGET}/app
.wsl-test-command:
	@echo "${BIN_PATH}"
	@echo ""

build-windows:
	cargo build --target x86_64-pc-windows-gnu ${OPT}
	@echo "> Windows Test command:"
	@OPT=${OPT} $(MAKE) .windows-test-command

.windows-test-command: BIN_PATH ?= \\\\wsl$$\\${WSL_DISTRO_NAME}$(shell echo ${HOST_WORKSPACE_FOLDER} | sed 's|/|\\\\|g')\\target\\x86_64-pc-windows-gnu\\${TARGET}\\app.exe
.windows-test-command:
	@echo "copy ${BIN_PATH} .\; .\app.exe"
	@echo ""

docker-build:
	@docker build -t ${DOCKER_IMAGE} .
	@docker images \
		--format 'Docker image created: {{.CreatedSince}}, size: {{.Size}}' \
		${DOCKER_IMAGE}

docker-run:
	@docker run --rm ${DOCKER_IMAGE}

run-help:
	cargo run -- -h

run-bin: BIN_PATH ?= target/x86_64-unknown-linux-musl/${TARGET}/app
run-bin:
	@${BIN_PATH}

clippy:
	cargo clippy
