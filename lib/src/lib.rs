use anstyle::AnsiColor;
use anstyle::Effects;
use anstyle::Style;
use clap::{value_parser, Arg, ArgMatches, Command};
use clap_cargo::style;
use std::path::PathBuf;
use std::process::ExitCode;

const STYLE_HEADER: Style = AnsiColor::Blue.on_default().effects(Effects::BOLD);
const STYLE_USAGE: Style = AnsiColor::Blue.on_default().effects(Effects::BOLD);

pub const CLAP_STYLING: clap::builder::styling::Styles = clap::builder::styling::Styles::styled()
    .header(STYLE_HEADER)
    .usage(STYLE_USAGE)
    .literal(style::LITERAL)
    .placeholder(style::PLACEHOLDER)
    .error(style::ERROR)
    .valid(style::VALID)
    .invalid(style::INVALID);

pub fn base_cmd() -> Command {
    Command::new("")
        .styles(CLAP_STYLING)
        .subcommand_required(false)
}

pub fn example_cmd() -> Command {
    Command::new("example").arg(
        Arg::new("some-arg")
            .short('s')
            .long("some-arg")
            .value_name("VALUE")
            .env("SOME_ARG")
            .value_parser(value_parser!(PathBuf)),
    )
}

pub fn example_handle(sub_matches: &ArgMatches) -> ExitCode {
    if let Some(some_arg) = sub_matches.get_one::<PathBuf>("some-arg") {
        println!("Some arg: {:?}", some_arg);
    } else {
        println!("No arg provided");
    }

    ExitCode::FAILURE
}
