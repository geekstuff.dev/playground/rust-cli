use std::{env, process::ExitCode};

enum Env {
    Devcontainer,
    WindowsSubsystemLinux,
    Windows,
    Unknown,
}

fn main() -> ExitCode {
    println!(">> Start Rust CLI app");
    match environment() {
        Env::Devcontainer => libdevcontainer::command(),
        Env::WindowsSubsystemLinux => libwsl::command(),
        Env::Windows => libwindows::command(),
        _ => {
            println!("> Undetected environment");
            println!("error: Could not detect environment.");
            ExitCode::FAILURE
        }
    }
}

fn environment() -> Env {
    if in_devcontainer() {
        Env::Devcontainer
    } else if in_wsl() {
        Env::WindowsSubsystemLinux
    } else if in_windows() {
        Env::Windows
    } else {
        Env::Unknown
    }
}

fn in_devcontainer() -> bool {
    env::var("REMOTE_CONTAINERS").map_or(false, |v| v == "true")
}

fn in_wsl() -> bool {
    std::fs::read_to_string("/proc/version").map_or(false, |release_info| {
        release_info.to_lowercase().contains("microsoft")
    })
}

fn in_windows() -> bool {
    env::consts::OS == "windows"
}
