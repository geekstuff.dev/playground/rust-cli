use std::process::ExitCode;

pub fn command() -> ExitCode {
    let command = lib::base_cmd().subcommand(lib::example_cmd());
    let matches = command.get_matches();
    match matches.subcommand() {
        Some(("example", sub_matches)) => lib::example_handle(sub_matches),
        _ => execute_default(),
    }
}

fn execute_default() -> ExitCode {
    println!("> Windows environment");
    ExitCode::SUCCESS
}
